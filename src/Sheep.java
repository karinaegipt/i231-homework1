
public class Sheep {

   enum Animal {sheep, goat}
   
   public static void main (String[] param) {
      // for debugging
   }
   
   public static void reorder (Animal[] animals) {
      // TODO!!! Your program here
      quickSort(animals, 0, animals.length-1);
   }

   public static void quickSort (Animal[] animals, int start, int end) {
      if (animals.length < 2) return; // if there is less than 2 elements in array sort isn't needed, break
      int iStart = start; // temporary start and end points
      int jEnd = end;

      do {
         while (animals[iStart] != Animal.sheep && iStart < end) iStart++; //iStart < end check needed if arrays contains only goat
         while ((animals [jEnd] == animals[iStart] || animals [jEnd] != Animal.goat) && jEnd > start ) jEnd--;
         if ( iStart <= jEnd) { //replace
            Animal tmp = animals[iStart];
            animals[iStart] = animals[jEnd];
            animals[jEnd] = tmp;
            iStart++; jEnd--;
         }
      } while (iStart < jEnd);
   }
}

